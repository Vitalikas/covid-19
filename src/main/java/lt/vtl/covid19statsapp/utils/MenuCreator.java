package lt.vtl.covid19statsapp.utils;

import java.util.ArrayList;
import java.util.List;

public class MenuCreator {
    List<String> options = new ArrayList<>();

    public List<String> createOptions() {
        options.add("Cancel and exit");
        options.add("Global statistics");
        options.add("Statistics by country");
        options.add("Country with the highest COVID-19 case number");
        options.add("TOP infected countries");
        options.add("Country place in Top infected countries");
        options.add("Country first case date");
        return options;
    }
}
