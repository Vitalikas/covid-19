package lt.vtl.covid19statsapp.task;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lt.vtl.covid19statsapp.model.Country;
import lt.vtl.covid19statsapp.model.CountryBySlug;
import lt.vtl.covid19statsapp.model.Covid19;
import lt.vtl.covid19statsapp.service.*;
import lt.vtl.covid19statsapp.utils.MenuCreator;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class Statistics {
    InputStream is = new ResourceFileReader().getFileFromResources();
    Map<String, String> paths = new ResourceFileReader().getApiPaths(is);

    JsonObject summaryObj = new JsonParser().getJsonData(paths.get("API_PATH_SUMMARY")).getAsJsonObject();
    Covid19 covid19 = new ClassObjectCreator<>(Covid19.class).create(summaryObj);
    // or Covid19 covid19 = new Gson().fromJson(summaryObj, Covid19.class);

    Search search = new Search();

    List<Country> countries = covid19.getCountries();

    public void selectStatistics() {
        IoService.showMessage("Choose option: ");
        int option = IoService.validateOptionInput(new MenuCreator().createOptions());

        switch (option) {
            case 1:
                showGlobalStatistics();
                showMoreStatistics();
                break;
            case 2:
                IoService.showMessage("\n0 - cancel and return");
                showCountryStatistics();
                showMoreStatistics();
                break;
            case 3:
                showCountryWithMaxTotalCases();
                showMoreStatistics();
                break;
            case 4:
                showTopInfectedCountries();
                showMoreStatistics();
                break;
            case 5:
                showCountryPlaceInTopInfectedCountries();
                showMoreStatistics();
                break;
            case 6:
                showCountryFirstCaseDate();
                showMoreStatistics();
                break;
            case 0:
                new CancellationService().cancelExit();
                break;
        }
    }

    private void showMoreStatistics() {
        boolean ok = IoService.confirmInput("\nGo for another statistics?");
        if (ok) {
            selectStatistics();
        } else {
            System.exit(0);
        }
    }

    private void showCountryFirstCaseDate() {
        CountryBySlug[] countriesBySlug = getCountriesBySlug();
        search.showFirstCaseDate(countriesBySlug);
    }

    public CountryBySlug[] getCountriesBySlug() {
        JsonArray countryObj = new JsonParser().getJsonData(
                new CountryUrl().createUrlBySlug(search.getCountrySlug(countries),
                        IoService.getDateFromInput(),
                        IoService.getDateToInput())).getAsJsonArray();
        return new ClassObjectCreator<>(CountryBySlug[].class).create(countryObj);
        // or CountryBySlug[] countryBySlug = new Gson().fromJson(countryObj, CountryBySlug[].class);
    }

    public void showCountryPlaceInTopInfectedCountries() {
        search.showCountryTopPlace(countries);
    }

    private void showTopInfectedCountries() {
        search.printTopInfectedCountries(countries);
    }

    private void showCountryWithMaxTotalCases() {
        search.showCountryWithMaxTotalCases(countries);
    }

    public void showCountryStatistics() {
        IoService.showMessage("\nEnter country name: ");
        String input = IoService.getUserInput();
        // cancelReturn() doesn't state for SOLID SRP
        if (input.equals("0")) {
            new CancellationService().cancelReturn();
        } else if (input.toLowerCase().equals("all")) {
            countries.forEach(System.out::println);
        } else {
            showStatisticsByCountryKey(input);
        }
    }

    private void showStatisticsByCountryKey(String key) {
        List<Country> countriesByKey = Search.getCountryByKey(countries, key);
        if (countriesByKey.size() == 0) {
            IoService.showMessage(key + " not found. Try again!");
            showCountryStatistics();
        } else {
            countriesByKey.forEach(System.out::println);
        }
    }

    private void showGlobalStatistics() {
        System.out.println(covid19.getGlobal());
    }
}