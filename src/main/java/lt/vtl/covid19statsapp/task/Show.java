package lt.vtl.covid19statsapp.task;

import lt.vtl.covid19statsapp.service.IoService;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Show {
    public void showGreetings() {
        IoService.showMessage("Welcome to COVID-19 statistics APP!\nStatistics available:\n");
    }

    public void showDataLoading() {
        IoService.showMessage("\n\nLoading data, please wait...\n");
    }

    public void showMenu(List<String> options) {
        AtomicInteger count = new AtomicInteger();
        options.forEach(o -> System.out.println(count.getAndIncrement() + ". " + o));
    }
}