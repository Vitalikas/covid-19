package lt.vtl.covid19statsapp.task;

import lt.vtl.covid19statsapp.model.Country;
import lt.vtl.covid19statsapp.model.CountryBySlug;
import lt.vtl.covid19statsapp.service.CancellationService;
import lt.vtl.covid19statsapp.service.IoService;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class Search {
    public static List<Country> getCountryByKey(List<Country> countries, String key) {
        List<Country> countryList = new ArrayList<>();
        countries.forEach(country -> {
            if (country.getCountryName().toLowerCase().contains(key.toLowerCase())) {
                countryList.add(country);
            }
        });
        return countryList;
    }

    public void showCountryTopPlace(List<Country> countries) {
        IoService.showMessage("\n0 - cancel and return");
        IoService.showMessage("\nEnter country name: ");
        String key = IoService.getUserInput().toLowerCase();
        // cancelReturn() doesn't state for SOLID SRP
        if (key.equals("0")) {
            new CancellationService().cancelReturn();
        } else {
            Map<Country, Integer> countriesPositions = getCountriesPositions(countries, key);
            showCountriesPositions(countriesPositions, countries);
//                IntStream.range(0, sortedCountries.size())
//                        .filter(i -> Objects.nonNull(sortedCountries.get(i)))
//                        .filter(i -> sortedCountries.get(i).getCountryName().toLowerCase().contains(key))
//                        .findFirst()
//                        .ifPresent(i -> System.out.printf("\n%s is in #%d/%d place " +
//                                        "according to the most infected country list\n",
//                            sortedCountries.get(i).getCountryName(), i + 1, sortedCountries.size()));
        }
    }

    public void showCountriesPositions(Map<Country, Integer> countriesPositions, List<Country> countries) {
        countriesPositions.forEach((country, position) ->
                System.out.printf("%s is in #%d/%d place according to the most infected country list\n",
                        country.getCountryName(), position + 1, countries.size()));
    }

    public static Map<Country, Integer> getCountriesPositions(List<Country> countries, String key) {
        Map<Country, Integer> countriesPositions = new HashMap<>();
        List<Country> countriesByKey = getCountryByKey(countries, key);
        if (countriesByKey.size() == 0) {
            IoService.showMessage(key + " not found. Try again!");
            new Statistics().showCountryPlaceInTopInfectedCountries();
        } else {
            List<Country> sortedCountries = countries.stream()
                    .sorted(Comparator.reverseOrder())
                    .collect(Collectors.toList());

            sortedCountries.forEach(country -> {
                if (country.getCountryName().toLowerCase().contains(key)) {
                    countriesPositions.put(country, sortedCountries.indexOf(country));
                }
            });
        }
        return countriesPositions;
    }

    public void showCountryWithMaxTotalCases(List<Country> countries) {
        countries.stream()
                .max(Comparator.comparingInt(Country::getTotalConfirmed))
                .ifPresent(System.out::println);
    }

    public void printTopInfectedCountries(List<Country> countries) {
        System.out.print("\nTOP number: ");
        int top = IoService.validateTopInput(countries);
        AtomicInteger num = new AtomicInteger();
        countries.stream()
                .sorted(Comparator.reverseOrder())
                .limit(top)
                .forEach(country -> System.out.println((num.getAndIncrement() + 1) + ". " + country));
    }

    public String getCountrySlug(List<Country> countries) {
        IoService.showMessage("\n0 - cancel and return");
        IoService.showMessage("\nEnter country name: ");
        String countrySlug = "";
        String key = IoService.getUserInput().toLowerCase();
        // cancelReturn() doesn't state for SOLID SRP
        if (key.equals("0")) {
            new CancellationService().cancelReturn();
        } else {
            countrySlug = getCountrySlugByKey(countries, key);
        }
        return countrySlug;
    }

    private String getCountrySlugByKey(List<Country> countries, String key) {
        AtomicReference<String> slug = new AtomicReference<>();
        countries.forEach(country -> {
            if (country.getSlug().toLowerCase().contains(key)) {
                Objects.requireNonNull(slug).set(country.getSlug());
            }
        });
        return slug.get();
    }

    public void showFirstCaseDate(CountryBySlug[] countriesBySlug) {
        Arrays.stream(countriesBySlug)
                .filter(record -> record.getCases() > 0)
                .findFirst()
                .ifPresent(countryBySlug -> System.out.printf("The first case of COVID-19 in %s confirmed on %s\n",
                        countryBySlug.getCountry(), countryBySlug.getDate()));
    }
}