package lt.vtl.covid19statsapp.model;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CountryBySlug {
    @SerializedName("Country")
    private final String country;

    @SerializedName("Confirmed")
    private final int confirmed;

    @SerializedName("Deaths")
    private final int death;

    @SerializedName("Recovered")
    private final int recovered;

    @SerializedName("Active")
    private final int active;

    @SerializedName("Date")
    private final Date date;

    @SerializedName("Cases")
    private final int cases;

    public CountryBySlug(String country, int confirmed, int death, int recovered, int active, Date date, int cases) {
        this.country = country;
        this.confirmed = confirmed;
        this.death = death;
        this.recovered = recovered;
        this.active = active;
        this.date = date;
        this.cases = cases;
    }

    public String getCountry() {
        return country;
    }

    public int getConfirmed() {
        return confirmed;
    }

    public int getDeath() {
        return death;
    }

    public int getRecovered() {
        return recovered;
    }

    public int getActive() {
        return active;
    }

    public String getDate() {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public int getCases() {
        return cases;
    }
}