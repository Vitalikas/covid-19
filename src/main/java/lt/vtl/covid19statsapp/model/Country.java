package lt.vtl.covid19statsapp.model;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Country implements Comparable<Country> {
    @SerializedName("Country")
    private final String countryName;

    @SerializedName("CountryCode")
    private final String countryCode;

    @SerializedName("Slug")
    private final String slug;

    @SerializedName("NewConfirmed")
    private final int newConfirmed;

    @SerializedName("TotalConfirmed")
    private final int totalConfirmed;

    @SerializedName("NewDeaths")
    private final int newDeaths;

    @SerializedName("TotalDeaths")
    private final int totalDeaths;

    @SerializedName("NewRecovered")
    private final int newRecovered;

    @SerializedName("TotalRecovered")
    private final int totalRecovered;

    @SerializedName("Date")
    private final Date date;

    public Country(String countryName, String countryCode, String slug, int newConfirmed, int totalConfirmed,
                   int newDeaths, int totalDeaths, int newRecovered, int totalRecovered, Date date) {
        this.countryName = countryName;
        this.countryCode = countryCode;
        this.slug = slug;
        this.newConfirmed = newConfirmed;
        this.totalConfirmed = totalConfirmed;
        this.newDeaths = newDeaths;
        this.totalDeaths = totalDeaths;
        this.newRecovered = newRecovered;
        this.totalRecovered = totalRecovered;
        this.date = date;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getSlug() {
        return slug;
    }

    public int getNewConfirmed() {
        return newConfirmed;
    }

    public int getTotalConfirmed() {
        return totalConfirmed;
    }

    public int getNewDeaths() {
        return newDeaths;
    }

    public int getTotalDeaths() {
        return totalDeaths;
    }

    public int getNewRecovered() {
        return newRecovered;
    }

    public int getTotalRecovered() {
        return totalRecovered;
    }

    public String getDate() {
        return new SimpleDateFormat("yyyy-MM-dd kk:mm:ss").format(date);
    }

    public String toString() {
        return String.format("\n%s\n" +
                "new cases: %d, total confirmed: %d\n" +
                "new deaths: %d, total death: %d\n" +
                "new recovered: %d, total recovered: %d\n" +
                "latest update date: %s",
                countryName,
                newConfirmed, totalConfirmed,
                newDeaths, totalDeaths,
                newRecovered, totalRecovered,
                getDate());
    }

    @Override
    public int compareTo(Country country) {
        return Integer.compare(this.totalConfirmed, country.totalConfirmed);
    }
}