package lt.vtl.covid19statsapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Covid19 {
    @SerializedName("Global")
    private final Global global;

    @SerializedName("Countries")
    private final List<Country> countries;

    public Covid19(Global global, List<Country> countries) {
        this.global = global;
        this.countries = countries;
    }

    public Global getGlobal() {
        return global;
    }

    public List<Country> getCountries() {
        return countries;
    }
}