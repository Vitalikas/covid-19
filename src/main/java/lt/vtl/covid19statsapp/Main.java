package lt.vtl.covid19statsapp;

import lt.vtl.covid19statsapp.task.Show;
import lt.vtl.covid19statsapp.task.Statistics;
import lt.vtl.covid19statsapp.utils.MenuCreator;

public class Main {
    public static void main(String[] args) {
        new Show().showGreetings();
        new Show().showMenu(new MenuCreator().createOptions());
        new Show().showDataLoading();
        new Statistics().selectStatistics();
    }
}