package lt.vtl.covid19statsapp.service;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

public class ClassObjectCreator<T> {
    final Class<T> type;

    public ClassObjectCreator(Class<T> type) {
        this.type = type;
    }

    public Class<T> getType() {
        return type;
    }

    public T create(JsonElement element) {
        return new Gson().fromJson(element, getType());
    }
}