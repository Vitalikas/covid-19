package lt.vtl.covid19statsapp.service;

import lt.vtl.covid19statsapp.model.Country;

import java.util.List;
import java.util.Scanner;

public class IoService {
    private static final Scanner scanner = new Scanner(System.in);

        public static String getUserInput() {
            return scanner.next();
    }

    public static String getDateFromInput() {
        System.out.print("Date from: ");
        return scanner.next();
    }

    public static String getDateToInput() {
        System.out.print("Date to: ");
        return scanner.next();
    }

    public static void showMessage(String message) {
        System.out.print(message);
    }

    public static boolean confirmInput(String text) {
        System.out.print(text + " [Y/N]: ");
        while (true) {
            String answer = scanner.next();
            if (answer.equalsIgnoreCase("Y")) {
                return true;
            } else if (answer.equalsIgnoreCase("N")) {
                return false;
            } else {
                System.out.println("Wrong input! Try again");
            }
        }
    }

    public static int validateOptionInput(List<String> options) {
        int num;
        do {
            while (!scanner.hasNextInt()) {
                String input = scanner.next();
                System.out.println(input + " is not a valid option! Please try again");
            }
            num = scanner.nextInt();
            if (num < 0) {
                System.out.println("Option must be positive! Please try again");
            } else if (num > options.size()) {
                System.out.println("There is no such option! Please try again");
            }
        } while (num < 0 || num > options.size());
        return num;
    }

    public static int validateTopInput(List<Country> countries) {
        int num;
        do {
            while (!scanner.hasNextInt()) {
                String input = scanner.next();
                System.out.println(input + " is not a valid option! Please try again");
            }
            num = scanner.nextInt();
            if (num < 0) {
                System.out.println("Option must be positive! Please try again");
            } else if (num > countries.size()) {
                System.out.println("There is no such option! Please try again");
            }
        } while (num < 0 || num > countries.size());
        return num;
    }

//    public static String checkInput(String input) {
//        for (int i = 0; i < input.length(); i++) {
//            while (input.substring(i).chars().allMatch(Character::isDigit)) {
//                IoService.showMessage("Wrong input! Try again!\n");
//                input = IoService.getUserInput();
//            }
//        }
//        return input;
//    }
}