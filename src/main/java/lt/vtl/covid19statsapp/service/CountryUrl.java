package lt.vtl.covid19statsapp.service;

import java.io.InputStream;
import java.util.Map;

public class CountryUrl {
    public String createUrlBySlug(String slug, String from, String to) {
        InputStream is = new ResourceFileReader().getFileFromResources();
        Map<String, String> paths = new ResourceFileReader().getApiPaths(is);
        return String.format("%s%s%s%s%s%s%s%s",
                paths.get("API_PATH"),
                slug,
                paths.get("FROM_PATH"),
                from,
                paths.get("TIME_ZONE"),
                paths.get("TO_PATH"),
                to,
                paths.get("TIME_ZONE")
                );
    }
}