package lt.vtl.covid19statsapp.service;

import lt.vtl.covid19statsapp.task.Statistics;

public class CancellationService {
    public void cancelExit() {
        boolean ok = IoService.confirmInput("\nAre you sure you want to cancel and exit?");
        if (ok) {
            System.exit(0);
        } else {
            new Statistics().selectStatistics();
        }
    }

    public void cancelReturn() {
        boolean ok = IoService.confirmInput("\nAre you sure you want to cancel and return?");
        if (ok) {
            new Statistics().selectStatistics();
        } else {
            new Statistics().showCountryStatistics();
        }
    }
}