package lt.vtl.covid19statsapp.service;

import javax.annotation.Nonnull;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class ResourceFileReader {
    // Getting file resources as stream from folder "resources"
    public InputStream getFileFromResources() {
        InputStream is = getClass().getClassLoader().getResourceAsStream("database.properties");
        if (is == null) {
            throw new IllegalStateException("File is not found!");
        } else {
            return is;
        }
    }

    // Reading file and getting paths
    public Map<String, String> getApiPaths(@Nonnull InputStream is) {
        Objects.requireNonNull(is, "File is not found!");
        Map<String, String> paths = new HashMap<>();
        try(BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            String line;
            String key;
            String value;
            while ((line = br.readLine()) != null) {
                String[] strings = line.split(" = ");
                key = strings[0];
                value = strings[1];
                paths.put(key, value);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return paths;
    }
}