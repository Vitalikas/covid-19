package lt.vtl.covid19statsapp.service;

import com.google.gson.JsonElement;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class JsonParser {
    public JsonElement getJsonData(String API_PATH) {
        JsonElement element = null;
        try {
            // Making connection
            URL url = new URL(API_PATH);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // Getting JSON data from API and converting to JSON element
            element = com.google.gson.JsonParser.parseReader(new InputStreamReader((InputStream) connection.getContent()));
        } catch (IOException e) {
            System.out.println("API server is not responding. Try again later.");
        }
        return element;
    }
}